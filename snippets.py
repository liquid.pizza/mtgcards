import mtgsdk
import pandas as pd

# get all cards
cards = mtgsdk.Card.all()

# Create DataFrame for rarity.
# Index is the set_name
rarity = [[len(df[(df["set_name"] == s) & (df["rarity"] == r)]) for r in df["rarity"].unique()] for s in df["set_name"].unique()]

df_rarity = pd.DataFrame(rarity, columns=df["rarity"].unique(), index=df["set_name"].unique())

# DataFrame for sets
sets = mtgsdk.Set.all()
df_sets = pd.DataFrame([[s.release_date, s.type, s.code, s.booster] for s in sets], columns=["release_date", "type", "code", "booster"], index=[s.name for s in sets])
df_sets["release_date"] = pd.to_datetime(df_sets["release_date"])